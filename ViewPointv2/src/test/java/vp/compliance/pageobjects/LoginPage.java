package vp.compliance.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
WebDriver ldriver;
	
	public LoginPage(WebDriver rdriver)
	{
		ldriver=rdriver;
		PageFactory.initElements(rdriver, this);
	}
	
	@FindBy(className="mat-input-element mat-form-field-autofill-control ng-tns-c56-3 cdk-text-field-autofill-monitored ng-untouched ng-pristine ng-invalid")
	@CacheLookup
	WebElement Email;
	
	@FindBy(className="mat-raised-button mat-primary")
	@CacheLookup
	WebElement Next;
	
	@FindBy(className="mat-input-element mat-form-field-autofill-control ng-tns-c56-4 cdk-text-field-autofill-monitored ng-untouched ng-pristine ng-invalid")
	@CacheLookup
	WebElement Password;
	
	@FindBy(className="mat-raised-button mat-primary cdk-focused cdk-mouse-focused")
	@CacheLookup
	WebElement Submit;

public void setEmail(String loginid)
{
	Email.sendKeys(loginid);
}

public void setPassword(String Pwd)
{
	Password.sendKeys(Pwd);
}
public void clickNext()
{
	Next.click();
}
public void clickSubmit()
{
	Submit.click();
}
}