package vp.compliance.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PortalsList {

	WebDriver ldriver;
	
	public PortalsList(WebDriver rdriver)
	{
		ldriver=rdriver;
		PageFactory.initElements(rdriver, this);
	}
	
	@FindBy(linkText="Compliance")
	@CacheLookup
	WebElement Compliancelink;
		
	 public void clickCompliance()
	 {
		 Compliancelink.click();
	 }
	 
	 @FindBy(linkText="AIMS")
	 @CacheLookup
	 WebElement AIMSlink;
	 public void clickAIMS()
	 {
		 AIMSlink.click();
	 }
//Find brilinta link 
	 @FindBy(linkText="Brilinta")
	 @CacheLookup
	 WebElement Brilintalink;
	 //Action method to click brilinta link
	 public void clickBrilinta()
	 {
		 Brilintalink.click();
	 }
	 
	 @FindBy(linkText="Diabetes Franchise")
	 @CacheLookup
	 WebElement DiabetesFranchiselink;
	 public void clickDiabetesFranchise()
	 {
		 Brilintalink.click();
	 }
	 
	 @FindBy(linkText="Speaker Management")
	 @CacheLookup
	 WebElement SpeakerManagementlink;
	 public void clickSpeakerManagement()
	 {
		 SpeakerManagementlink.click();
	 }
	 
	 @FindBy(linkText="Oncology")
	 @CacheLookup
	 WebElement Oncologylink;
	 public void clickOncology()
	 {
		 Oncologylink.click();
	 }
	 
}
