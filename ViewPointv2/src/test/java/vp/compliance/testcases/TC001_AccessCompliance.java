package vp.compliance.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;
import vp.compliance.pageobjects.PortalsList;

public class TC001_AccessCompliance extends BaseClass

{
	
	@Test
	public void AccessCompliance() throws IOException
	
	{
		driver.get(IntranetURL);
		logger.info("Accessed Intranet Dev NewIG");
		PortalsList pl=new PortalsList(driver);
		logger.info("Accessed Portal List");
		pl.clickCompliance();
		captureScreen(driver,"CompliancePortal");
		logger.info("Accessed Compliance portal");
		
		boolean Page=driver.getPageSource().contains("Welcome");
		if (Page==true)
			
		{
			Assert.assertTrue(true);
			logger.info("Landed on Compliance portal");
		}
		else
		{
			captureScreen(driver,"AccessCompliance");
			Assert.assertTrue(false);
			logger.info("Did not land on Compliance portal");
		}
	
	}
		
}
