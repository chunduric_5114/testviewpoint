package vp.compliance.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import vp.compliance.pageobjects.PortalsList;

public class TC003_AccessSpeakerManagement extends BaseClass{
	@Test
	public void AccessSpeakerManagement() throws IOException
	
	{
		driver.get(IntranetURL);
		logger.info("Accessed Intranet Dev NewIG");
		PortalsList pl=new PortalsList(driver);
		logger.info("Accessed Portal List");
		pl.clickSpeakerManagement();
		captureScreen(driver,"SpeakerManagementPortal");
		logger.info("Accessed SpeakerManagement portal");
		
		boolean Page=driver.getPageSource().contains("SPEAKER");
		if (Page==true)
			
		{
			Assert.assertTrue(true);
			logger.info("Landed on SPEAKER MANAGEMENT portal");
		}
		else
		{
			captureScreen(driver,"SpeakerManagementPortal");
			Assert.assertTrue(false);
			logger.info("Did not land on SPEAKER MANAGEMENT portal");
		}
	
	}
		
}
