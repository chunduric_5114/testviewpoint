package vp.compliance.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import vp.compliance.pageobjects.PortalsList;

public class TC004_AcessDiabetesFranchise extends BaseClass{
	@Test
	public void AcessDiabetesFranchise() throws IOException
	
	{
		driver.get(IntranetURL);
		logger.info("Accessed Intranet Dev NewIG");
		PortalsList pl=new PortalsList(driver);
		logger.info("Accessed Portal List");
		pl.clickDiabetesFranchise();
		captureScreen(driver,"DiabetesFranchisePortal");
		logger.info("Accessed Diabetes Franchise portal");
		
		boolean Page=driver.getPageSource().contains("Diabetes");
		if (Page==true)
			
		{
			Assert.assertTrue(true);
			logger.info("Landed on Diabetes Franchise portal");
		}
		else
		{
			captureScreen(driver,"DiabetesFranchisePortal");
			Assert.assertTrue(false);
			logger.info("Did not land on Diabetes Franchise portal");
		}
	
	}
		
}