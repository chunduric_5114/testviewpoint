package vp.compliance.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import vp.compliance.pageobjects.PortalsList;

public class TC007_AccessRespiratory extends BaseClass{
	@Test
	public void AccessOncology() throws IOException
	
	{
		driver.get(IntranetURL);
		logger.info("Accessed Intranet Dev NewIG");
		PortalsList pl=new PortalsList(driver);
		logger.info("Accessed Portal List");
		pl.clickRespiratory();
		captureScreen(driver,"RespiratoryPortal");
		logger.info("Accessed Respiratory portal");
		
		boolean Page=driver.getPageSource().contains("Respiratory");
		if (Page==true)
			
		{
			Assert.assertTrue(true);
			logger.info("Landed on Respiratory portal");
		}
		else
		{
			captureScreen(driver,"RespiratoryPortal");
			Assert.assertTrue(false);
			logger.info("Did not land on Respiratory portal");
		}
	
	}
		
}