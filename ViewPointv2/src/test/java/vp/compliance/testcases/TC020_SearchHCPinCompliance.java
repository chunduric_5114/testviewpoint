package vp.compliance.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import vp.compliance.pageobjects.PortalsList;
import vp.compliance.pageobjects.Professionals;
import vp.compliance.utilities.XLUtils;

public class TC020_SearchHCPinCompliance extends BaseClass{

	@Test(dataProvider="HCP")
	public void searchHCP(String ID) throws IOException
	{
		driver.get(IntranetURL);
		logger.info("Accessed Intranet Dev NewIG");
		PortalsList pl=new PortalsList(driver);
		logger.info("Accessed Portal List");
		pl.clickCompliance();
		Professionals HCP=new Professionals(driver);
		HCP.clickProfessionals();
		logger.info("Landed on professionals tab");
		HCP.clickClear();
		logger.info("Cleared Search criteria");
/*		HCP.clickID();
		logger.info("Clicked on ID criteria");*/
		HCP.selectAZID();
		logger.info("Selected dropdown value for ID type");
		HCP.enterID(ID);
		logger.info("Entered Search Criteria for ID");
		HCP.clickSearch();
		logger.info("Search Results");
		boolean Page=driver.getPageSource().contains("match found");
		if (Page==true)
			
		{
			Assert.assertTrue(true);
			logger.info("HCP is found");
		}
		else
		{
			captureScreen(driver,"searchHCP");
			Assert.assertTrue(false);
			logger.info("HCP is not found");
		}
	}


	
@DataProvider(name="HCP")
String [][] getData() throws IOException
{
	String path=System.getProperty("user.dir")+"/src/test/java/vp/compliance/testdata/AZID_not in VP.xlsx";
	int rownum=XLUtils.getRowCount(path, "Sheet1");
	int colcount=XLUtils.getCellCount(path, "Sheet1", 1);
	
	String HCPid[][]=new String[rownum][colcount];
	for (int i=1;i<=rownum;i++)
	{
		for(int j=0;j<colcount;j++)
		{
			HCPid[i-1][j]=XLUtils.getCellData(path, "Sheet1", i, j);//1,0
		}
	}
	return HCPid;
}
}